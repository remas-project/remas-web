import { createTheme, Theme } from "@material-ui/core";

export const CustomTheme: Theme = createTheme({
    palette: {
        primary: {
            main: '#2196f3'
        }
    }
});