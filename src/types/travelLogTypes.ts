export interface TravelLog {
    id?: string;
    travelDate: string;
    description: string;
    miles: number;
}